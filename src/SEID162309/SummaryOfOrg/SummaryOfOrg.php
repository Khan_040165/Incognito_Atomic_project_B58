<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 5/29/2017
 * Time: 6:06 PM
 */

namespace App\SummaryOfOrg;

use App\Message\Message;
use App\Model\Database;
use PDO;

class SummaryOfOrg extends Database
{

    public $id;
    public $orgName;
    public $summary;


    public function setData($postArray){

        if(array_key_exists("id",$postArray)){
            $this->id = $postArray['id'];
        }

        if(array_key_exists("orgName",$postArray)){
            $this->orgName = $postArray['orgName'];
        }

        if(array_key_exists("summary",$postArray)){
            $this->summary = $postArray['summary'];
        }

    }// end of setData()



    public function store(){

        $orgName = $this->orgName;
        $summary = $this->summary;

        $sqlQuery = "INSERT INTO `org_summary` ( `org_name`, `org_summary`) VALUES ( ?, ?);";
        $dataArray = array($orgName,$summary) ;

        $STH = $this->DBH->prepare($sqlQuery);


        $result = $STH->execute($dataArray);


        if($result){
            Message::message("Success! Data has been inserted Successfully!");
        }
        else{
            Message::message("Error! Data has not been inserted.");

        }



    }// end of store()

    public function index(){

        $sqlQuery = "select * from org_summary WHERE is_trashed = 'NO'";

        $STH = $this->DBH->query($sqlQuery);

        $STH->setFetchMode(PDO::FETCH_OBJ);
        $allData = $STH->fetchAll();

        return $allData;

    }//index() end

    public function view(){

        $sqlQuery = "select * from org_summary WHERE id=".$this->id;


        $STH = $this->DBH->query($sqlQuery);

        $STH->setFetchMode(PDO::FETCH_OBJ);
        $singleData = $STH->fetch();

        return $singleData;

    }

    public function update(){

        $orgName = $this->orgName;
        $summary = $this->summary;

        $sqlQuery = "UPDATE org_summary SET org_name = ?,org_summary=? WHERE id =$this->id" ;
        $dataArray = array($orgName,$summary) ;

        $STH = $this->DBH->prepare($sqlQuery);


        $result = $STH->execute($dataArray);


        if($result){
            Message::message("Success! Data has been updated Successfully!");
        }
        else{
            Message::message("Error! Data has not been updated.");

        }

    }// end of update()

    public function trash(){

        $sqlQuery = "UPDATE org_summary SET is_trashed = NOW() WHERE id =$this->id" ;

         $result = $this->DBH->exec($sqlQuery);


        if($result){
            Message::message("Success! Data has been updated Successfully!");
        }
        else{
            Message::message("Error! Data has not been updated.");

        }

    }//end of trash()

    public function trashed(){

        $sqlquery = "SELECT * FROM org_summary WHERE is_trashed <> 'NO';";

        $STH = $this->DBH->query($sqlquery);

        $STH->setFetchMode(PDO::FETCH_OBJ);
        $allData = $STH->fetchAll();
        return $allData;

    }//trashed()







}// end of SummaryOfOrg Class








