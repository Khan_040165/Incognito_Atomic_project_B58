<?php
namespace App\DateOfBirth;

use PDO;
use App\Message\Message;
use App\Model\Database;

class Dob extends Database
{
    public $id;
    public $userName;
    public $calender;




    public function setDobData($postArray){

        if (array_key_exists('id',$postArray)){

            $this->id = $postArray['id'];
        }

        if (array_key_exists('userName',$postArray)){

            $this->userName = $postArray['userName'];
        }

        if (array_key_exists('calender',$postArray)){

            $this->calender = $postArray['calender'];
        }


    }

    public function store(){
        $userName = $this->userName;

        $calender = $this->calender;

        $sqlQuery = "INSERT INTO dob (user_name,dob) VALUES (?,?);";

        $STH = $this->DBH->prepare($sqlQuery);
        $dataArray = array($userName,$calender);

        $result = $STH->execute($dataArray);

        if ($result){

            Message::message("Success!! data has been inserted");
        }else{
            Message::message("Error!! Data has not been inserted");
        }


    }//store();



    public function index(){

        $sqlQuery = "select * from dob WHERE is_trashed= 'NO'";

        $STH = $this->DBH-> query($sqlQuery);

        $STH ->setFetchMode(PDO::FETCH_OBJ);
        $allData = $STH->fetchAll();

        return $allData;
    }//index()


    public function view(){
        $sqlQuery = "select * from dob where id= " .$this->id;

        $STH = $this->DBH->query($sqlQuery);

        $STH->setFetchMode(PDO::FETCH_OBJ);
        $singleData = $STH->fetch();
        return $singleData;

    }//view();


    public function update(){

        $sqlQuery = "UPDATE dob SET user_name = ?, dob = ? WHERE dob . id = $this->id;";
        $dataArray = array($this->userName, $this->calender);

        $STH = $this->DBH->prepare($sqlQuery);
        $result = $STH->execute($dataArray);

        if($result){
            Message::message("Success :) Data has been updated successfully.");
        }
        else {
            Message::message("Failure :( Data has not been updated due to an error.");

        }

    }//update()

    public function trash(){

        $sqlQuery = "UPDATE dob SET is_trashed = NOW() WHERE dob . id = $this->id;";

        $result = $this->DBH->exec($sqlQuery);

        if($result){
            Message::message("Success :) Data has been trashed successfully.");
        }
        else {
            Message::message("Failure :( Data has not been sent to trashed due to an error.");

        }

    }//trash()


    public function trashed(){

        $sqlQuery = "select * from dob WHERE is_trashed <> 'NO'";

        $STH = $this->DBH-> query($sqlQuery);

        $STH ->setFetchMode(PDO::FETCH_OBJ);
        $allData = $STH->fetchAll();

        return $allData;
    }


    public function recover(){

        $sqlQuery = "UPDATE dob SET is_trashed = 'NO' WHERE dob . id = $this->id;";

        $result = $this->DBH->exec($sqlQuery);

        if($result){
            Message::message("Success :) Data has been recovered from trashed successfully.");
        }
        else {
            Message::message("Failure :( Data has not been recovered due to an error.");

        }

    }//recover()

    public function delete(){

        $sqlQuery = "DELETE FROM dob WHERE  id = $this->id;";

        $result = $this->DBH->exec($sqlQuery);

        if($result){
            Message::message("Success :) Data has been recovered from trashed successfully.");
        }
        else {
            Message::message("Failure :( Data has not been recovered due to an error.");

        }

    }






}//dob class end