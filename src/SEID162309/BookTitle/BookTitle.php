<?php
/**
 * Created by PhpStorm.
 * User: Trainer 402
 * Date: 5/28/2017
 * Time: 12:38 PM
 */

namespace App\BookTitle;


use PDO;
use App\Message\Message;
use App\Model\Database;

class BookTitle extends Database
{
      public $id;
      public $bookTitle;
      public $authorName;


      public function setData($postArray){


            if(array_key_exists("id",$postArray)){
                  $this->id = $postArray['id'];
            }

            if(array_key_exists("bookName",$postArray)){
                  $this->bookTitle = $postArray['bookName'];
            }

            if(array_key_exists("authorName",$postArray)){
                  $this->authorName = $postArray['authorName'];
            }

      }// end of setData()



      public function store(){

             $bookTitle = $this->bookTitle;
             $authorName = $this->authorName;

             $sqlQuery = "INSERT INTO `book_title` ( `book_title`, `author_name`) VALUES ( ?, ?);";
             $dataArray = array($bookTitle,$authorName) ;

             $STH = $this->DBH->prepare($sqlQuery);


             $result = $STH->execute($dataArray);


             if($result){
                   Message::message("Success! Data has been inserted Successfully!");
             }
            else{
                   Message::message("Error! Data has not been inserted.");

            }



      }// end of store()

      public function index(){

          $sqlQuery = "select * from book_title WHERE is_trashed = 'NO'";

          $STH = $this->DBH->query($sqlQuery);

          $STH->setFetchMode(PDO::FETCH_OBJ);
          $allData = $STH->fetchAll();

          return $allData;

      }

    public function view(){

        $sqlQuery = "select * from book_title WHERE id=".$this->id;


        $STH = $this->DBH->query($sqlQuery);

        $STH->setFetchMode(PDO::FETCH_OBJ);
        $singleData = $STH->fetch();

        return $singleData;

    }

    public function update(){

        $bookTitle = $this->bookTitle;
        $authorName = $this->authorName;

        $sqlQuery = "UPDATE book_title SET book_title = ?,author_name=? WHERE id =$this->id" ;
        $dataArray = array($bookTitle,$authorName) ;

        $STH = $this->DBH->prepare($sqlQuery);


        $result = $STH->execute($dataArray);


        if($result){
            Message::message("Success! Data has been updated Successfully!");
        }
        else{
            Message::message("Error! Data has not been updated");

        }



    }// end of update()

    public function trash(){

        $sqlQuery = "UPDATE book_title SET is_trashed = NOW() WHERE book_title . id = $this->id;";

        $result = $this->DBH->exec($sqlQuery );

        if($result){
            Message::message("Success :) Data has been trashed successfully.");
        }
        else {
            Message::message("Failure :( Data has not been sent to trashed folder due to an error.");

        }

    }//trash() end

    public function trashed(){
        $sqlQuery = "select * from book_title WHERE is_trashed <> 'NO'";

        $STH = $this->DBH-> query($sqlQuery);

        $STH ->setFetchMode(PDO::FETCH_OBJ);
        $allData = $STH->fetchAll();

        return $allData;

    }//end of trashed()


    public function recover(){

        $sqlQuery = "UPDATE book_title SET is_trashed = 'NO' WHERE book_title . id = $this->id;";

        $result = $this->DBH->exec($sqlQuery );

        if($result){
            Message::message("Success :) Data has been recovered successfully.");
        }
        else {
            Message::message("Failure :( Data has not been recovered due to an error.");

        }

    }//recover() end

    public function delete(){

        $sqlQuery = "DELETE from book_title WHERE id = $this->id;";

        $result = $this->DBH->exec($sqlQuery );

        if($result){
            Message::message("Success :) Data has been deleted successfully.");
        }
        else {
            Message::message("Failure :( Data has not been deleted due to an error.");

        }
    }//delete()









}// end of Book Title Class














