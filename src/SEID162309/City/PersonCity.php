<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 5/29/2017
 * Time: 3:36 AM
 */

namespace App\City;

use PDO;

use App\Message\Message;
use App\Model\Database;


class PersonCity extends Database
{
   public $personCity;
    public $id;
    public $personName;


    public function setData($postArray){


        if(array_key_exists("id",$postArray)){
            $this->id = $postArray['id'];
        }


        if(array_key_exists("personCity",$postArray)){
            $this->personCity = $postArray['personCity'];
        }
        if(array_key_exists("personName",$postArray)){
            $this->personName = $postArray['personName'];
        }


    }// end of setData()

    public function store(){

        $personCity = $this->personCity;
        $personName = $this->personName;

        $sqlQuery = "INSERT INTO person_city (person_name,city_name) VALUES ( ?, ?);";
        $dataArray = array($personName,$personCity) ;

        $STH = $this->DBH->prepare($sqlQuery);


        $result = $STH->execute($dataArray);


        if($result){
            Message::message("Success! Data has been inserted Successfully!");
        }
        else{
            Message::message("Error! Data has not been inserted.");

        }



    }// end of store()

    public function index(){

        $sqlQuery = "select * from person_city WHERE is_trashed = 'NO';";

        $STH = $this->DBH->query($sqlQuery);

        $STH->setFetchMode(PDO::FETCH_OBJ);
        $allData = $STH->fetchAll();

        return $allData;

    }

    public function view(){

        $sqlQuery = "select * from person_city WHERE id=".$this->id;


        $STH = $this->DBH->query($sqlQuery);

        $STH->setFetchMode(PDO::FETCH_OBJ);
        $singleData = $STH->fetch();

        return $singleData;

    }

    public function update(){

        $personName = $this->personName;
        $personCity = $this->personCity;

        $sqlQuery = "UPDATE person_city SET person_name = ?,city_name = ? WHERE id =$this->id " ;
        $dataArray = array($personName,$personCity) ;

        $STH = $this->DBH->prepare($sqlQuery);


        $result = $STH->execute($dataArray);


        if($result){
            Message::message("Success! Data has been updated Successfully!");
        }
        else{
            Message::message("Error! Data has not been updated");

        }



    }// end of update()

    public function trash(){


        $sqlQuery = "UPDATE person_city SET is_trashed = NOW() WHERE id = $this->id;";

        $result = $this->DBH->exec($sqlQuery);

        if($result){
            Message::message("Success :) Data has been trashed successfully.");
        }
        else {
            Message::message("Failure :( Data has not been trashed due to an error.");

        }
    }//trash()

    public function trashed(){

        $query = "SELECT * FROM person_city WHERE is_trashed <> 'NO'";

        $STH = $this->DBH->query($query);

        $STH->setFetchMode(PDO::FETCH_OBJ);
        $allData = $STH->fetchAll();
        return $allData;

    }//trashed()

    public function delete(){


        $sqlQuery = "DELETE FROM person_city  WHERE id = $this->id;";

        $result = $this->DBH->exec($sqlQuery);

        if($result){
            Message::message("Success :) Data has been removed successfully.");
        }
        else {
            Message::message("Failure :( Data has not been removed due to an error.");

        }
    }//delete()

    public function recover(){


        $sqlQuery = "UPDATE person_city SET is_trashed = 'NO' WHERE id = $this->id;";

        $result = $this->DBH->exec($sqlQuery);

        if($result){
            Message::message("Success :) Data has been trashed successfully.");
        }
        else {
            Message::message("Failure :( Data has not been trashed due to an error.");

        }
    }



}// end of Book city Class















