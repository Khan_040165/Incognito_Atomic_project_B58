<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 5/29/2017
 * Time: 1:57 PM
 */

namespace App\Gender;
use PDO;

use App\Message\Message;
use App\Model\Database;

class PersonGender extends Database
{
    public $personGender;
    public $personName;
    public $id;

    public function setData($postArray){

        if(array_key_exists("id",$postArray)){
            $this->id = $postArray['id'];
        }

        if(array_key_exists("selector",$postArray)){
            $this->personGender = $postArray['selector'];
        }

        if(array_key_exists("name",$postArray)){
            $this->personName = $postArray['name'];
        }


    }// end of setData()

    public function store(){

        $personGender = $this->personGender;
        $personName = $this->personName;

        $sqlQuery = "INSERT INTO person_gender (person_name,person_gender) VALUES ( ?, ?);";
        $dataArray = array($personName,$personGender) ;

        $STH = $this->DBH->prepare($sqlQuery);


        $result = $STH->execute($dataArray);


        if($result){
            Message::message("Success! Data has been inserted Successfully!");
        }
        else{
            Message::message("Error! Data has not been inserted.");

        }



    }// end of store()


    public function index(){

        $sqlQuery = "select * from person_gender";

        $STH = $this->DBH->query($sqlQuery);

        $STH->setFetchMode(PDO::FETCH_OBJ);
        $allData = $STH->fetchAll();

        return $allData;

    }


    public function view(){

        $sqlQuery = "select * from person_gender WHERE id=".$this->id;


        $STH = $this->DBH->query($sqlQuery);

        $STH->setFetchMode(PDO::FETCH_OBJ);
        $singleData = $STH->fetch();

        return $singleData;

    }

    public function update(){

        $personName = $this->personName;
        $personGender = $this->personGender;

        $sqlQuery = "UPDATE person_gender SET person_name = ?,person_gender=? WHERE id =$this->id" ;
        $dataArray = array($personName,$personGender) ;

        $STH = $this->DBH->prepare($sqlQuery);


        $result = $STH->execute($dataArray);


        if($result){
            Message::message("Success! Data has been updated Successfully!");
        }
        else{
            Message::message("Error! Data has not been updated.");

        }



    }
}// end of personGender Class











