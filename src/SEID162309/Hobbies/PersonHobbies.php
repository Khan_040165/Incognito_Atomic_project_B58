<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 5/29/2017
 * Time: 4:00 PM
 */

namespace App\Hobbies;

use App\Message\Message;
use App\Model\Database;
use PDO;

class PersonHobbies extends Database
{
    public $personHobby;
    public $personName;
    public $id;

    public function setData($postArray){

        if(array_key_exists("id",$postArray)){
            $this->id = $postArray['id'];
        }

        if(array_key_exists("name",$postArray)){
            $this->personName = $postArray['name'];
        }

        if(array_key_exists("hobby",$postArray)){
            $this->personHobby = $postArray['hobby'];
        }


    }// end of setData()

    public function store(){
        $personName = $this->personName;
        $personHobby = $this->personHobby;
        $personHobbies = implode(",",$personHobby);


        $sqlQuery = "INSERT INTO person_hobby (person_name,person_hobby) VALUES (?, ?);";


        $dataArray = array($personName,$personHobbies) ;

        $STH = $this->DBH->prepare($sqlQuery);


        $result = $STH->execute($dataArray);



        if($result){
            Message::message("Success! Data has been inserted Successfully!");
        }
        else{
            Message::message("Error! Data has not been inserted.");

        }



    }// end of store()

    public function index(){

        $sqlQuery = "select * from person_hobby";

        $STH = $this->DBH->query($sqlQuery);

        $STH->setFetchMode(PDO::FETCH_OBJ);
        $allData = $STH->fetchAll();

        return $allData;

    }

    public function view(){

        $sqlQuery = "select * from person_hobby WHERE id=".$this->id;


        $STH = $this->DBH->query($sqlQuery);

        $STH->setFetchMode(PDO::FETCH_OBJ);
        $singleData = $STH->fetch();

        return $singleData;

    }

    public function update(){

        $personName = $this->personName;
        $personHobby = $this->personHobby;
        $personHobbies = implode(",",$personHobby);


        $sqlQuery = "UPDATE person_hobby SET person_name = ?,person_hobby= ? WHERE id =$this->id" ;


        $dataArray = array($personName,$personHobbies) ;

        $STH = $this->DBH->prepare($sqlQuery);


        $result = $STH->execute($dataArray);



        if($result){
            Message::message("Success! Data has been updated Successfully!");
        }
        else{
            Message::message("Error! Data has not been updated.");

        }



    }



}// end of personHobby Class












