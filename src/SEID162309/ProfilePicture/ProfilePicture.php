<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 5/29/2017
 * Time: 3:16 PM
 */

namespace App\ProfilePicture;
use App\Utility\Utility;
use PDO;
use App\Message\Message;
use App\Model\Database;


class ProfilePicture extends Database
{

    public $id;
    public $name;
    public $profilePicture;
    public $image;


    public function setData($postArray){


        if(array_key_exists("id",$postArray)){
            $this->id = $postArray['id'];
        }

        if(array_key_exists("name",$postArray)){
            $this->name = $postArray['name'];
        }

        if(array_key_exists("profilePicture",$postArray)){
            $this->profilePicture = $postArray['profilePicture'];
        }

    }// end of setData()



    public function store(){

        $name = $this->name;
        $profilePicture = $this->profilePicture;

        $sqlQuery = "INSERT INTO `profile_picture` (`profile_name`, `profile_picture`) VALUES ( ?, ?);";
        $dataArray = array($name,$profilePicture) ;

        $STH = $this->DBH->prepare($sqlQuery);


        $result = $STH->execute($dataArray);


        if($result){
            Message::message("Success! Data has been inserted Successfully!");
        }
        else{
            Message::message("Error! Data has not been inserted.");

        }

    }// end of store()


    public function index(){

        $sqlQuery = "select * from profile_picture";

        $STH = $this->DBH->query($sqlQuery);

        $STH->setFetchMode(PDO::FETCH_OBJ);
        $allData = $STH->fetchAll();

        return $allData;

    }

    public function view(){

        $sqlQuery = "select * from profile_picture WHERE id=".$this->id;


        $STH = $this->DBH->query($sqlQuery);

        $STH->setFetchMode(PDO::FETCH_OBJ);
        $singleData = $STH->fetch();

        return $singleData;


    }


    public function update(){

        $profileName = $this->name;
        $profilePicture = $this->profilePicture;

        $sqlQuery = "UPDATE profile_picture SET profile_name= ?,profile_picture=? WHERE id =$this->id" ;
        $dataArray = array($profileName,$profilePicture) ;


        $STH = $this->DBH->prepare($sqlQuery);


        $result = $STH->execute($dataArray);


        if($result){
            Message::message("Success! Data has been updated Successfully!");
        }
        else{
            Message::message("Error! Data has not been updated");

        }



    }// end of update()


// end of ProfilePicture Class

}