<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Atomic Project </title>

    <!-- Bootstrap Core CSS -->
    <link href="resource/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Theme CSS -->
    <link href="resource/bootstrap/css/freelancer.min.css" rel="stylesheet">
    <link href="resource/bootstrap/css/freelancer.min.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="resource/bootstrap/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body id="page-top" class="index" style="background-color: #1abc9c">
<div id="skipnav"><a href="#maincontent">Skip to main content</a></div>

<!-- Navigation -->
<nav id="mainNav" class="navbar navbar-default  navbar-custom" style="padding-top: 0px;height:120px ">
    <div class="container" style="padding-top: 2px;">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header page-scroll">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
            </button>
            <h4 style="padding-bottom: 50px"> <a class="navbar-brand" href="#page-top" style=""><span style="color: red">Atomic &nbsp;</span><span style="color: green"> project</span></a></h4>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1" style="" >
            <div style="padding-top: 40px">
            <ul class="nav navbar-nav navbar-right" style="font-style: oblique">
                <li class="page-scroll">
                    <a href="index.php">Home</a>
                </li>
                <li class="page-scroll">
                    <a href="views/SEID162309/BookTitle/index.php">Book Title</a>
                </li>
                <li class="page-scroll">
                    <a href="views/SEID162309/ProfilePicture/index.php">Profile Picture</a>
                </li>
                <li class="page-scroll">
                    <a href="views/SEID162309/Gender/index.php">Gender</a>
                </li>
                <li class="page-scroll">
                    <a href="views/SEID162309/City/index.php">City</a>
                </li>
                <li class="page-scroll">
                    <a href="views/SEID162309/Hobbies/index.php">Hobbies</a>
                </li>
                <li class="page-scroll">
                    <a href="views/SEID162309/Email/index.php">Email</a>
                </li>
                <li class="page-scroll">
                    <a href="views/SEID162309/Date of Birth/create.php">Birthday</a>
                </li>
                <li class="page-scroll">
                    <a href="views/SEID162309/SummaryOfOrg/index.php">Summary Of Org</a>
                </li>
            </ul>
            </div>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
</nav>

<!-- Header -->
<header>
    <div class="container-fluid" id="maincontent" style="padding-top: 25px" >
        <div class="row">
            <div class="col-lg-3">
                <img class="img-responsive img-circle" style="height: 260px;width: 270px" src="resource/img/muhtasim.jpg" alt="">
                <h5 style="font-weight: bold;color: lavender;font-size: 18px">Muhtasim Chowdhury</h5>
            </div>&nbsp;&nbsp;&nbsp;
            <div class="col-lg-3">
                <img class="img-responsive img-circle" style="height: 260px;width: 270px" src="resource/img/zarif.jpg" alt="">
                <h5 style="font-weight: bold;color: lavender;font-size: 18px">Zarif Rahman</h5>
            </div>
            <div class="col-lg-3">
                <img class="img-responsive img-circle" style="height: 260px;width: 270px" src="resource/img/ridwan.jpg" alt="">
                <h5 style="font-weight: bold;color: lavender;font-size: 18px">ridwanur khan</h5>
            </div>
            <div class="col-lg-3">
                <img class="img-responsive img-circle" style="height: 240px;width: 270px" src="resource/img/sourav.jpg" alt="">
                <h5 style="font-weight: bold;color: lavender;font-size: 18px">sourav biswas</h5>
            </div>
                <div class="intro-text">
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <h2 class="name" style="color: ">Team Incognito</h2>
                    <hr class="star-light">

                </div>
            </div>
        </div>

</header>

<!-- Footer -->
<footer class="text-center">

    <div class="footer-below">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    Copyright &copy; Your Website 2017
                </div>
            </div>
        </div>
    </div>
</footer>


<!-- jQuery -->
<script src="resource/bootstrap/js/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="resource/bootstrap/js/bootstrap.min.js"></script>

<!-- Plugin JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>


</body>

</html>
