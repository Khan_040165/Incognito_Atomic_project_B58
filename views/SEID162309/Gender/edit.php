<?php

require_once("../../../vendor/autoload.php");

$obj = new \App\Gender\PersonGender();

$obj->setData($_GET);
$singleData = $obj->view();

$genderString = $singleData->person_gender;

use App\Message\Message;

$msg = Message::message();
if($msg!='') {
    echo "<div><center>  <div class='' style='width: 390px;height: 35px;background-color: #2b669a;font-weight: bold;color: white;font-size: 20px;padding-left: 80px;padding-right: 70px;border-radius: 4px' id='message'>  $msg </div></center> </div>";
}
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Book Title Add Form</title>

    <link rel="stylesheet" href="../../../resource/bootstrap/css/radioStyle.css">

</head>
<body>

    <h1 style="color: aliceblue;">Person Gender Information </h1>

    <form action="update.php" method="post">

        <div class="container">

            <h1 style="color: white">Choose Gender :</h1><br>
            <lebel style="font-size: 25px;color:white">Name :</lebel>
            <input style="height: 35px;width: 400px;border-radius: 3px;font-size: 20px;background-color: antiquewhite" type="text" name="name" value="<?php echo $singleData->person_name?>">
            <hr>
            <ul>
                <li>
                    <input type="radio" value="Male" <?php if($genderString=="Male") echo "checked";?> id="f-option" name="selector">
                    <label for="f-option">Male</label>

                    <div class="check"></div>
                </li>

                <li>
                    <input type="radio" value="Female" <?php if($genderString=="Female") echo "checked";?> id="s-option" name="selector">
                    <label for="s-option">Female</label>

                    <div class="check"><div class="inside"></div></div>
                </li>

                <li>
                    <input type="radio" value="Others" <?php if($genderString=="Others") echo "checked";?> id="t-option" name="selector">
                    <label for="t-option">Others</label>

                    <div class="check"><div class="inside"></div></div>
                </li>
            </ul>
            <input type="hidden" name="id" value="<?php echo $singleData->id?>">
            <br>
            <input style="width: 100px;height: 35px;font-size: 20px;border-radius: 6px" type="submit" value="Submit">
        </div>

    </form>


</body>
<script src="../../../resource/bootstrap/js/jquery.js"></script>

<script>


    jQuery(

        function($) {
            $('#message').fadeOut (800);
            $('#message').fadeIn (800);
            $('#message').fadeOut (800);
            $('#message').fadeIn (800);
            $('#message').fadeOut (800);
            $('#message').fadeIn (800);
            $('#message').fadeOut (800);
        }
    )
</script>

</html>