<?php
use App\Utility\Utility;

require_once("../../../vendor/autoload.php");

$objPersonGender  = new App\Gender\PersonGender();

$objPersonGender->setData($_POST);

$objPersonGender->store();

Utility::redirect('create.php');