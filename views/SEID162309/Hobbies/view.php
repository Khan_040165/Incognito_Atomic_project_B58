<?php
require_once ("../../../vendor/autoload.php");


$obj = new \App\Hobbies\PersonHobbies();
$obj->setData($_GET);
$singleData = $obj->view();


?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <title>Document</title>
</head>
<body>
<center><h2> Single Data Information-Person Hobby</h2></center><br>
<table class="table table-bordered table-striped">


    <tr>
        <th>ID</th>
        <th>Person Name</th>
        <th>Hobbies</th>
    </tr>
    <?php

        echo "
   <tr>
      
      <td>$singleData->id</td>
      <td>$singleData->person_name</td>
      <td>$singleData->person_hobby</td>
      <td><a href='index.php'><button class='btn btn-primary'>Back</button></a></td>
      
   </tr>

";

    ?>



</table>

</body>
</html>
