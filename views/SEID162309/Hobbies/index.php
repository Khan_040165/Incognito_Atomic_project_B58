<?php
require_once ("../../../vendor/autoload.php");
use App\Message\Message;

$obj = new \App\Hobbies\PersonHobbies();
$allData = $obj->index();

$msg = Message::message();
if($msg!='') {
    echo "<div><center>  <div class='' style='width: 500px;height: 35px;background-color: #2b669a;font-weight: bold;color: white;font-size: 20px;padding-left: 80px;padding-right: 70px;border-radius: 4px' id='message'>  $msg </div></center> </div>";
}

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>
    <script src="../../../resource/bootstrap/js/jquery.js"></script>
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <script src="../../../resource/bootstrap/js/bootbox.min.js"></script>
    <script src="../../../resource/bootstrap/js/jquery.confirm.min.js"></script>
    <script src="../../../resource/bootstrap/js/jquery.confirm.js"></script>
    <title>Document</title>
</head>
<body>
<center><h2> Multiple Data Information-Person Hobby</h2></center><br>
<table class="table table-bordered table-striped">


    <tr>
        <th>Serial</th>
        <th>ID</th>
        <th>Person Name</th>
        <th>Hobbies</th>
        <th>Action</th>
    </tr>
<?php
$serial = 1;
foreach ($allData as $result){
    echo "
   <tr>
      <td>$serial</td>
      <td>$result->id</td>
      <td>$result->person_name</td>
      <td>$result->person_hobby</td>
      <td>
         <a href='view.php?id=$result->id'><button class='btn btn-primary'>View</button></a>
         <a href='edit.php?id=$result->id'><button class='btn btn-success'>Edit</button></a>
         <a id='alert' href='' da ><button class='btn btn-danger ' data-toggle=''>Delete</button></a>
      </td>
   </tr>

";

    $serial++;
}

?>



</table>

</body>
<script src="../../../resource/bootstrap/js/jquery.js"></script>

<script>


    jQuery(

        function($) {
            $('#message').fadeOut (800);
            $('#message').fadeIn (800);
            $('#message').fadeOut (800);
            $('#message').fadeIn (800);
            $('#message').fadeOut (800);
            $('#message').fadeIn (800);
            $('#message').fadeOut (800);
        }
    )
</script>

</html>
