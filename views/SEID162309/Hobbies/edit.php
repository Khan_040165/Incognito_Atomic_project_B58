<?php

require_once("../../../vendor/autoload.php");

$obj = new \App\Hobbies\PersonHobbies();

$obj->setData($_GET);
$singleData = $obj->view();


$hobbyArray = explode(",",$singleData->person_hobby);

use App\Message\Message;

$msg = Message::message();
if($msg!='') {
    echo "<div><center>  <div class='' style='width: 390px;height: 35px;background-color: #2b669a;font-weight: bold;color: white;font-size: 20px;padding-left: 80px;padding-right: 70px;border-radius: 4px' id='message'>  $msg </div></center> </div>";
}
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Book Title Add Form</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="../../../resource/bootstrap/js/jquery.js"></script>

    <style>
        body {
            background:aliceblue;
        }
    </style>
</head>
<body>
<br><br><br><br><br>
<div class="container" style="width: 500px;height: 400px;background-color: #8c8c8c;border-radius: 8px">
    <h2 style="font-weight: bold">Choose Your Hobby</h2><br><br>


    <form action="update.php" method="post">
        <lebel style="font-size: 25px;color:white">Name :</lebel>
        <input style="height: 35px;width: 300px;border-radius: 3px;font-size: 20px;background-color: antiquewhite" type="text" name="name" value="<?php echo $singleData->person_name?>"><br><br>

        <label class="checkbox-inline">
            <input type="checkbox" value="Programming" name="hobby[]" <?php if(in_array("Programming",$hobbyArray)) echo "checked"?>>Programming
        </label>
        <label class="checkbox-inline">
            <input type="checkbox" value="Dancing" name="hobby[]"<?php if(in_array("Dancing",$hobbyArray)) echo "checked"?>>Dancing
        </label>
        <label class="checkbox-inline">
            <input type="checkbox" value="Singing" name="hobby[]"<?php if(in_array("Singing",$hobbyArray)) echo "checked"?>>Singing
        </label>
        <br><br>
        <label class="checkbox-inline">
            <input type="checkbox" value="Cricket" name="hobby[]"<?php if(in_array("Cricket",$hobbyArray)) echo "checked"?>>Cricket
        </label>
        <label class="checkbox-inline">
            <input type="checkbox" value="Writing" name="hobby[]"<?php if(in_array("Writing",$hobbyArray)) echo "checked"?>>Writing
        </label>
        <label class="checkbox-inline">
            <input type="checkbox" value="Gardenning" name="hobby[]"<?php if(in_array("Gardenning",$hobbyArray)) echo "checked"?>>Gardenning
        </label>
        <br><br><br>
        <input type="hidden" name="id" value="<?php echo $singleData->id?>">
        <input style="height: 40px;font-size: 20px;width: 100px" type="submit" value="Submit">
    </form>
</div>

</body>

<script>


    jQuery(

        function($) {
            $('#message').fadeOut (800);
            $('#message').fadeIn (800);
            $('#message').fadeOut (800);
            $('#message').fadeIn (800);
            $('#message').fadeOut (800);
            $('#message').fadeIn (800);
            $('#message').fadeOut (800);
        }
    )
</script>

</html>