<?php
use App\Utility\Utility;

require_once("../../../vendor/autoload.php");

$objProfilePicture  = new App\ProfilePicture\ProfilePicture();

$fileName = time(). $_FILES['image'] ['name'];

$source = $_FILES['image'] ['tmp_name'];
$destination = "images/".$fileName;

$_POST['profilePicture'] = $fileName;

move_uploaded_file($source, $destination);

$objProfilePicture->setData($_POST);

$objProfilePicture->store();

Utility::redirect('create.php');