<?php
require_once ("../../../vendor/autoload.php");

use App\Utility\Utility;
$obj = new \App\ProfilePicture\ProfilePicture();
$obj->setData($_GET);
$singleData = $obj->view();


?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <title>Document</title>
</head>
<body>
<center><h2> Single Data Information-Profile Picture</h2></center><br>
<table class="table table-bordered table-striped">


    <tr>
        <th>ID</th>
        <th>Book Title</th>
        <th>Author Name</th>
    </tr>
    <?php

        echo "
   <tr>
      
      <td>$singleData->id</td>
      <td>$singleData->profile_name</td>
      <td><img src='images/$singleData->profile_picture' height='100px' width='100px'></td>
      <td><a href='index.php'><button class='btn btn-primary'>Back</button></a></td>
      
   </tr>

";

    ?>



</table>

</body>
</html>
