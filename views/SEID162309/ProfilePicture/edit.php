<?php

require_once("../../../vendor/autoload.php");

$obj = new App\ProfilePicture\ProfilePicture();

$obj->setData($_GET);
$singleData = $obj->view();

use App\Message\Message;

$msg = Message::message();
if($msg!='') {
    echo "<div><center>  <div class='' style='width: 390px;height: 35px;background-color: #2b669a;font-weight: bold;color: white;font-size: 20px;padding-left: 80px;padding-right: 70px;border-radius: 4px' id='message'>  $msg </div></center> </div>";
}
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Book Title Add Form</title>
    <link rel="stylesheet" href="../../../resource/bootstrap/css/formstyle.css">
    
    <style>
        body {
            background:antiquewhite;
        }
    </style>
</head>
<body>
<div class="container">
    <h1 style="color: #442a8d">Profile Picture</h1>

    <form class="form-group " action="update.php" method="post" enctype="multipart/form-data">
        Enter Your Name :
        <br>
        <input class="form-control" type="text" name="name" value="<?php echo $singleData->profile_name?>">
        <br>
        Choose Your Profile Picture :
        <input type="file" name="image" accept=".png,.jpg,.jpeg">
        <img class="img-responsive" src="images/<?php echo $singleData->profile_picture?>" alt="image" height="300px" width="300px">
        <br>
        <input type="hidden" name="id" value="<?php echo $singleData->id?>">
        <input type="submit" value="Update Profile Picture">
        <br>

    </form>
</div>


</body>
<script src="../../../resource/bootstrap/js/jquery.js"></script>

<script>


    jQuery(

        function($) {
            $('#message').fadeOut (800);
            $('#message').fadeIn (800);
            $('#message').fadeOut (800);
            $('#message').fadeIn (800);
            $('#message').fadeOut (800);
            $('#message').fadeIn (800);
            $('#message').fadeOut (800);
        }
    )
</script>

</html>