<?php

require_once("../../../vendor/autoload.php");
if(!isset($_SESSION)) session_start();

use App\Message\Message;
$msg = Message::message();

if($msg!='')
echo "<div><center>  <div class='' style='width: 390px;height: 35px;background-color: #2b669a;font-weight: bold;color: white;font-size: 20px;padding-left: 80px;padding-right: 70px;border-radius: 4px' id='message'>  $msg </div></center> </div>";

?>



<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/formstyle.css">
    <title>Document</title>
</head>
<body>

<div class="container">
    <h1 style="color: #442a8d">Profile Picture</h1>

    <form class="form-group " action="store.php" method="post" enctype="multipart/form-data">
        Enter Your Name :
        <br>
        <input class="form-control" type="text" name="name">
        <br>
        Choose Your Profile Picture :
        <input type="file" name="image" accept=".png,.jpg,.jpeg">
        <br>
        <input type="submit">
        <br>

    </form>
</div>
    <script src="../../../resource/bootstrap/js/jquery.js"></script>

    <script>


        jQuery(

            function($) {
                $('#message').fadeOut (800);
                $('#message').fadeIn (800);
                $('#message').fadeOut (800);
                $('#message').fadeIn (800);
                $('#message').fadeOut (800);
                $('#message').fadeIn (800);
                $('#message').fadeOut (800);
            }
        )
    </script>

</body>
</html>