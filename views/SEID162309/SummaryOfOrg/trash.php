<?php
require_once ('../../../vendor/autoload.php');
use App\Utility\Utility;

$objSumTrash = new App\SummaryOfOrg\SummaryOfOrg();

$objSumTrash->setData($_GET);

$objSumTrash->trash();

Utility::redirect('trashed.php');