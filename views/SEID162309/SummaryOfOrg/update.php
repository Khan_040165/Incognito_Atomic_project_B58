<?php
use App\Utility\Utility;

require_once("../../../vendor/autoload.php");


$objBookTitle  = new App\SummaryOfOrg\SummaryOfOrg();

$objBookTitle->setData($_POST);

$objBookTitle->update();

Utility::redirect('index.php');