<?php
use App\Utility\Utility;

require_once("../../../vendor/autoload.php");

$objSummaryOfOrg  = new App\SummaryOfOrg\SummaryOfOrg();

$objSummaryOfOrg->setData($_POST);

$objSummaryOfOrg->store();

Utility::redirect('index.php');