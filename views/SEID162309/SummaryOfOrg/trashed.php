<?php
	require_once ("../../../vendor/autoload.php");
use App\Message\Message;

    $objSum = new App\SummaryOfOrg\SummaryOfOrg();
	$allData = $objSum-> trashed();


	$msg = Message::message();

	    echo "<div class='container_alert text-center'>
<div id = 'message' class='alert alert-success'>
<span class='glyphicon glyphicon-ok-sign'>

</span> $msg
</div>
</div>";
?>



<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport"
	      content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>View  trashed List Of Company Tale</title>
	<link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="../../../resource/bootstrap/css/index_style.css">
</head>
<body>
<div class="container_view">
	<h1 class="text-center text-capitalize">multiple  trashed list of Company Tale</h1>
	<div class="col-sm-8 col-sm-offset-2">

    <table class="table table-bordered table-striped table-condensed table-responsive">
	    <tr>
		    <th>Serial</th>
		    <th>ID</th>
		    <th>Company name</th>
		    <th>Description</th>
		    <th>Action Buttons</th>
	    </tr>


<?php

	$serial= 1;

	foreach ($allData as $result){
	echo"
		<tr>
		<td>$serial</td>
		<td>$result->id</td>
		<td>$result->org_name</td>
		<td>$result->org_summary</td>
		<td>
		  <a href='view.php?id=$result->id'><button class='btn btn-info btn-sm'>View</button></a>
		  <a href='edit.php?id=$result->id'><button class='btn btn-primary  btn-sm'>EDIT</button></a>
		  <a href='recover.php?id=$result->id'><button class='btn btn-success  btn-sm'>Recover item</button></a>
		  <a href='delete.php?id=$result->id'><button class='btn btn-danger  btn-sm'>Delete</button></a>
		
		  
		
		</td>
		
		</tr>
		";
		$serial++;
	}



?>

    </table>


	</div>
</div>
<script src="../../../resource/bootstrap/js/jquery.js"></script>
<script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>
</body>

<script type="text/javascript">

    jQuery(

        function($) {
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
        }
    )


</script>

    </html>
