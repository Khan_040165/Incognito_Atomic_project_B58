<?php

require_once("../../../vendor/autoload.php");

$obj = new \App\SummaryOfOrg\SummaryOfOrg();

$obj->setData($_GET);
$singleData = $obj->view();

$stringOrg  =  $singleData->org_summary;

use App\Message\Message;

$msg = Message::message();
if($msg!='') {
    echo "<div><center>  <div class='' style='width: 390px;height: 35px;background-color: #2b669a;font-weight: bold;color: white;font-size: 20px;padding-left: 80px;padding-right: 70px;border-radius: 4px' id='message'>  $msg </div></center> </div>";
}
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Book Title Add Form</title>
    <link rel="stylesheet" href="../../../resource/bootstrap/css/formstyle.css">
    <script src="../../../resource/bootstrap/js/jquery.js"></script>

    <style>
        body {
            background:antiquewhite;
        }
    </style>
</head>
<body>
<div class="container">

    <h1 style="color: #442a8d;">Summary of Organization</h1>

    <form action="update.php" method="post">

        <strong> Organization Name:</strong>
        <input type="text" name="orgName" value="<?php echo $singleData->org_name?>">
        <br>
        <strong> Summary :</strong>
        <br><textarea name="summary" rows="10" cols="55" ><?php echo $stringOrg;?></textarea>
        <br><br><br>

        <input type="hidden" name="id" value="<?php echo $singleData->id?>">

        <input type="submit" value="Submit">

    </form>


</div>
</body>
<script src="../../../resource/bootstrap/js/jquery.js"></script>

<script>


    jQuery(

        function($) {
            $('#message').fadeOut (800);
            $('#message').fadeIn (800);
            $('#message').fadeOut (800);
            $('#message').fadeIn (800);
            $('#message').fadeOut (800);
            $('#message').fadeIn (800);
            $('#message').fadeOut (800);
        }
    )
</script>

</html>