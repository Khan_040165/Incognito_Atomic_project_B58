<?php

use App\Utility\Utility;

require_once ("../../../vendor/autoload.php");

$objCityName = new App\City\PersonCity();

$objCityName->setData($_POST);

$objCityName->store();

Utility::redirect('index.php');