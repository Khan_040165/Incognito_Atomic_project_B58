<?php
require_once ("../../../vendor/autoload.php");
use App\Message\Message;

$obj = new App\City\PersonCity();
$allData = $obj->index();
$msg = Message::message();
if($msg!='') {
    echo "<div>
<center> 
 <div style='width: 500px;height:
  35px;background-color: #2b669a;
  font-weight: bold;color: white;
  font-size: 20px;
  padding-left: 80px;
  padding-right: 70px;
  border-radius: 4px' id='message'>
  $msg
   </div>
  </center>
   </div>";
}

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>
    <script src="../../../resource/bootstrap/js/jquery.js"></script>
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <script src="../../../resource/bootstrap/js/bootbox.min.js"></script>
    <script src="../../../resource/bootstrap/js/jquery.confirm.min.js"></script>
    <script src="../../../resource/bootstrap/js/jquery.confirm.js"></script>
    <title>Document</title>
</head>
<body>
<center><h2> Multiple Data Information-Person City</h2></center><br>
<div class="container_table">
    <div class="row">
        <div class="col-sm-8 col-sm-offset-2">

    <table class="table table-bordered table-striped table-responsive table-condensed">
     <tr style= 'text-align:center;'>
        <th>Serial</th>
        <th>ID</th>
        <th>Person Name</th>
        <th>City Name</th>
        <th>Action</th>
    </tr>
<?php
$serial = 1;
foreach ($allData as $result){
    echo "
   <tr align='center'>
      <td>$serial</td>
      <td>$result->id</td>
      <td>$result->person_name</td>
      <td>$result->city_name</td>
      <td>
         <a href='view.php?id=$result->id'><button class='btn btn-primary btn-sm'>View</button></a>
         <a href='edit.php?id=$result->id'><button class='btn btn-success btn-sm'>Edit</button></a>
         <a href='trash.php?id=$result->id'><button class='btn btn-warning btn-sm'>Trash</button></a>
         <a id='alert' href='delete.php'><button class='btn btn-danger btn-sm ' data-toggle=''>Delete</button></a>
      </td>
   </tr>

";

    $serial++;
}

?>
</table>
        </div>
    </div>
</div>
<script src="../../../resource/bootstrap/js/jquery.js"></script>
</body>


<script>


    jQuery(

        function($) {
            $('#message').fadeOut (800);
            $('#message').fadeIn (800);
            $('#message').fadeOut (800);
            $('#message').fadeIn (800);
            $('#message').fadeOut (800);
            $('#message').fadeIn (800);
            $('#message').fadeOut (800);
        }
    )
</script>

</html>
