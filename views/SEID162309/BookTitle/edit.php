<?php

require_once("../../../vendor/autoload.php");
use App\Message\Message;

$objEdit = new \App\BookTitle\BookTitle();



$objEdit->setData($_GET);
$singleData = $objEdit->view();



$msg = Message::message();
if($msg!='') {
    echo "<div><center>  <div class='' style='width: 390px;height: 35px;background-color: #2b669a;font-weight: bold;color: white;font-size: 20px;padding-left: 80px;padding-right: 70px;border-radius: 4px' id='message'>  $msg </div></center> </div>";
}
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Book Title Add Form</title>

    <link rel="stylesheet" href="../../../resource/bootstrap/css/form-style-10.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <link href="../../../resource/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <link href="../../../resource/bootstrap/css/freelancer.min.css" rel="stylesheet">

    <link href="../../../resource/bootstrap/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->


    <style>
        body {
            background:;
        }
    </style>
</head>
<body>

<nav id="mainNav" class="navbar navbar-default  navbar-custom" style="padding-top: 0px;height: 120px">
    <div class="container" style="padding-top: 2px;">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header page-scroll">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
            </button>
            <h4 style="padding-bottom: 50px"> <a class="navbar-brand" href="#page-top" style=""><span style="color: red">Atomic &nbsp;</span><span style="color: green"> project</span></a></h4>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1" style="" >
            <div style="padding-top: 40px">
                <ul class="nav navbar-nav navbar-right" style="font-style: oblique">
                    <li class="page-scroll">
                        <a href="../../../index.php">Home</a>
                    </li>
                    <li class="page-scroll">
                        <a href="index.php">Book Title</a>
                    </li>
                    <li class="page-scroll">
                        <a href="../ProfilePicture/index.php">Profile Picture</a>
                    </li>
                    <li class="page-scroll">
                        <a href="../Gender/index.php">Gender</a>
                    </li>
                    <li class="page-scroll">
                        <a href="../City/index.php">City</a>
                    </li>
                    <li class="page-scroll">
                        <a href="../Hobbies/index.php">Hobbies</a>
                    </li>
                    <li class="page-scroll">
                        <a href="../Email/index.php">Email</a>
                    </li>
                    <li class="page-scroll">
                        <a href="../Birthday/index.php">Birthday</a>
                    </li>
                    <li class="page-scroll">
                        <a href="../SummaryOfOrg/index.php">Summary Of Org</a>
                    </li>
                </ul>
            </div>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
</nav>

<div class="container">
<div class="form-style-10">
    <h1 style="color: #442a8d;">Book Information Entry</h1>

    <form action="update.php" method="post">

        <div style="" class="section"><span>1</span>Book name & Author Name</div>
        <div class="inner-wrap">
            <label style="font-size: 18px;">Enter Book Name <input style="color: black" type="text" name="bookName" value="<?php echo $singleData->book_title?>" /></label>
            <label style="font-size: 18px">Enter Author Name <input style="color: black" type="text"  name="authorName" value="<?php echo $singleData->author_name?>"/></label>
            <input type="hidden" name="id" value="<?php echo $singleData->id?>">
        </div>


        <div class="button-section">
            <br>  <input type="submit" name="submit" value="Update" />
            <span class="privacy-policy">

     </span>
        </div>

    </form>

</div>
</div>


<!-- Footer -->
<footer class="text-center">

    <div class="footer-below">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    Copyright &copy; Your Website 2017
                </div>
            </div>
        </div>
    </div>
</footer>

</body>
<script src="../../../resource/bootstrap/js/jquery.js"></script>

<script>


    jQuery(

        function($) {
            $('#message').fadeOut (800);
            $('#message').fadeIn (800);
            $('#message').fadeOut (800);
            $('#message').fadeIn (800);
            $('#message').fadeOut (800);
            $('#message').fadeIn (800);
            $('#message').fadeOut (800);
        }
    )
</script>
<script src="../../../resource/bootstrap/js/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>

<!-- Plugin JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>

</html>