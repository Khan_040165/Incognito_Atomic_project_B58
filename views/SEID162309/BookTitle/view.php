<?php
require_once ("../../../vendor/autoload.php");


$obj = new \App\BookTitle\BookTitle();
$obj->setData($_GET);
$singleData = $obj->view();


?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <link href="../../../resource/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <link href="../../../resource/bootstrap/css/freelancer.min.css" rel="stylesheet">

    <link href="../../../resource/bootstrap/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js" type="text/javascript"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>

    <title>Document</title>
</head>

<style>
    .th{
        text-align: center;
    }
</style>
<body>

<nav id="mainNav" class="navbar navbar-default  navbar-custom" style="padding-top: 0px;height: 120px">
    <div class="container" style="padding-top: 2px;">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header page-scroll">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
            </button>
            <h4 style="padding-bottom: 50px"> <a class="navbar-brand" href="#page-top" style=""><span style="color: red">Atomic &nbsp;</span><span style="color: green"> project</span></a></h4>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1" style="" >
            <div style="padding-top: 40px">
                <ul class="nav navbar-nav navbar-right" style="font-style: oblique">
                    <li class="page-scroll">
                        <a href="../../../index.php">Home</a>
                    </li>
                    <li class="page-scroll">
                        <a href="index.php">Book Title</a>
                    </li>
                    <li class="page-scroll">
                        <a href="../ProfilePicture/index.php">Profile Picture</a>
                    </li>
                    <li class="page-scroll">
                        <a href="../Gender/index.php">Gender</a>
                    </li>
                    <li class="page-scroll">
                        <a href="../City/index.php">City</a>
                    </li>
                    <li class="page-scroll">
                        <a href="../Hobbies/index.php">Hobbies</a>
                    </li>
                    <li class="page-scroll">
                        <a href="../Email/index.php">Email</a>
                    </li>
                    <li class="page-scroll">
                        <a href="../Birthday/index.php">Birthday</a>
                    </li>
                    <li class="page-scroll">
                        <a href="../SummaryOfOrg/index.php">Summary Of Org</a>
                    </li>
                </ul>
            </div>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
</nav><br><br>
<hr>
<div class="container">
    <div class="btn-group ">

        <a href="create.php"><button type="button" class="btn btn-info">Add New Book</button></a>
        <a><button type="button" class="btn btn-danger">View Trashed List</button></a>
        <a><button type="button" class="btn btn-primary">Delete Selected</button></a>
        <a><button type="button" class="btn btn-info">Trashed Selected</button></a>
        <a><button type="button" class="btn btn-success">Download as PDF</button></a>
        <a><button type="button" class="btn btn-warning">Download as XL</button></a>
        <a href=""> <button type="button" class="btn btn-primary">Send an Email</button></a>
    </div>
</div>
<hr>

<div class="container" style="padding: 30px;">

<center><h2> Single Data Information-Book Title</h2></center><br>
<table class="table table-bordered table-striped">


    <tr style="font-weight: bold;font-size: 18px">
        <th class="th">ID</th>
        <th  class="th">Book Title</th>
        <th  class="th">Author Name</th>
        <th  class="th">Action</th>
    </tr>
    <?php

        echo "
   <tr align='center'>
      
      <td>$singleData->id</td>
      <td>$singleData->book_title</td>
      <td>$singleData->author_name</td>
      <td>
      <a href='index.php'><button class='btn btn-primary'>Back</button></a>
      <a href='edit.php'><button class='btn btn-success'>Edit</button></a>
      <a href='delete.php'><button class='btn btn-danger'>Delete</button></a>
      </td>
      
   </tr>

";

    ?>



</table>
</div>
<!-- Footer -->
<footer class="text-center navbar-fixed-bottom">

    <div class="footer-below" style="margin-top: 320px;">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    Copyright &copy; Your Website 2017
                </div>
            </div>
        </div>
    </div>
</footer>

</body>
<script src="../../../resource/bootstrap/js/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>

<!-- Plugin JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>

</html>
