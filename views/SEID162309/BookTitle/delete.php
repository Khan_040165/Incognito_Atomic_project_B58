<?php
$path = $_SERVER['HTTP_REFERER'];
require_once ("../../../vendor/autoload.php");

use App\Utility\Utility;

$objDelete = new App\BookTitle\BookTitle();
$objDelete->setData($_GET);

$objDelete->delete();

Utility::redirect($path);