<?php

require_once ("../../../vendor/autoload.php");
use App\Utility\Utility;

$objTrash = new App\BookTitle\BookTitle();

$objTrash->setData($_GET);

$objTrash->trash();

Utility::redirect('trashed.php');