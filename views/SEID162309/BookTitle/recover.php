<?php

require_once ("../../../vendor/autoload.php");
use App\Utility\Utility;

$objTrash = new App\BookTitle\BookTitle();

$objTrash->setData($_GET);

$objTrash->recover();

Utility::redirect('index.php');