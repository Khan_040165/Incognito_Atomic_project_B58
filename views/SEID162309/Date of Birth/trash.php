<?php
require_once ('../../../vendor/autoload.php');
use App\Utility\Utility;

$objdobTrash = new App\DateOfBirth\Dob();

$objdobTrash->setDobData($_GET);

$objdobTrash->trash();

Utility::redirect('trashed.php');