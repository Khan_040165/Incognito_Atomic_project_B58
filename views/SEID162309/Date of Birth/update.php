<?php
require_once ("../../../vendor/autoload.php");

use App\Utility\Utility;

$objDob = new App\DateOfBirth\Dob();

$objDob->setDobData($_POST);

$objDob->update();

Utility::redirect('index.php');

