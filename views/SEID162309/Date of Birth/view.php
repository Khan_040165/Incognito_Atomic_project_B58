<?php
	require_once ("../../../vendor/autoload.php");

	$objSingleData = new App\DateOfBirth\Dob();

	$objSingleData->setdobData($_GET);

	$singleData = $objSingleData->view();
	?>


<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport"
	      content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>View List Of Book</title>
	<link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="../../../resource/bootstrap/css/index_style.css">
</head>
<body>
 <div class="container_view">
	 <h1 class="text-center text-capitalize">single data of book</h1>
	 <div class="col-sm-4 col-sm-offset-4">

<table class="table table-bordered table-striped table-condensed table-responsive">
	<tr>

		<th>ID</th>
		<th>user name</th>
		<th>Date of birth</th>

	</tr>


	<?php

		echo"
		<tr>
		
		<td>$singleData->id</td>
		<td>$singleData->user_name</td>
		<td>$singleData->dob</td>
		</tr>
		";
	?>
</table>
	 </div>
 </div>
<script src="../../../resource/bootstrap/js/jquery.js"></script>
<script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>
</body>
</html>

