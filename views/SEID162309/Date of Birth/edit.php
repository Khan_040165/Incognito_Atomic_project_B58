<?php
require_once "../../../vendor/autoload.php";
if (!isset($_SESSION))session_start();

use App\Message\Message;

$objDob = new App\DateOfBirth\Dob();

$objDob->setDobData($_GET);
$singleData = $objDob->view();

$msg = Message::message();

echo "<div><div id='success_message'>$msg</div></div>";

?>

<!DOCTYPE HTML>
<html>
<head>
    <meta charset="UTF-8">
    <title>jQuery</title>

    <link rel="stylesheet" href="../../../resource/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap-theme.min.css">
    <link  rel="stylesheet" type="text/css" href="../../../resource/jquery-ui-1.12.1/jquery-ui.css">
    <link  rel="stylesheet" type="text/css" href="../../../resource/bootstrap/css/date.css">

</head>
<body>


<div class="overlay">
    <div class="container">
        <div class="row">
            <form action="update.php" method="post" role="form" class="form-horizontal">
                <div class="col-sm-3 col-sm-offset-3">

                    <div class="form-group hidden">
                        <label for="id" class="control-label"></label>
                        <input type="hidden" class="form-control" name="id" value="<?php echo $singleData->id?>" id="id"/>
                    </div>

                    <div class="form-group">
                        <label for="userName" class="control-label">User Name</label>
                        <input type="text" class="form-control" name="userName" value="<?php echo $singleData->user_name?>" id="userName"/>
                    </div>

                    <label for="calender" class="control-label">Date of Birth</label>
                    <div class="input-group ui-datepicker-title-buttonpane">
                        <input type="text" class="form-control" id="dateTimepicker" name="calender" value="<?php echo $singleData->dob?>" placeholder="dd/mm/yy">  <span class="input-group-addon">
                    <span class="fa fa-calendar">
                    </span>
                </span>
                    </div>
                    <button type="submit" class="btn btn-success btn-date">Update</button>

            </form>
        </div>

    </div>

</div>



<script src="../../../resource/bootstrap/js/jquery.js"></script>
<script src="../../../resource/jquery-ui-1.12.1/jquery-ui.js"></script>
<script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>
<script src="../../../resource/bootstrap/js/date.js"></script>



</body>
</html>

