-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 03, 2017 at 07:40 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `atomic_project_001`
--
CREATE DATABASE IF NOT EXISTS `atomic_project_001` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `atomic_project_001`;

-- --------------------------------------------------------

--
-- Table structure for table `book_title`
--

CREATE TABLE `book_title` (
  `id` int(100) NOT NULL,
  `book_title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `author_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `book_title`
--

INSERT INTO `book_title` (`id`, `book_title`, `author_name`) VALUES
(1, 'Web development PHP', 'sergeon kelvin '),
(22, 'Computer Network', 'Andraw s. Tanenbaum'),
(23, 'Artificial Intelligence', 'Rich Knight'),
(24, 'C programming', 'Shaum''s outline'),
(25, 'Java Programming', 'B.  Gaushamy'),
(26, 'Javascript Tutorial', 'Michel Jhonson'),
(27, 'Common Mistake', 'Dr. jahingir sattar');

-- --------------------------------------------------------

--
-- Table structure for table `org_summary`
--

CREATE TABLE `org_summary` (
  `id` int(100) NOT NULL,
  `org_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `org_summary` varchar(256) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `org_summary`
--

INSERT INTO `org_summary` (`id`, `org_name`, `org_summary`) VALUES
(1, 'Kb & ZZ Groups', 'It''s a group of company..'),
(2, 'S. K. Technology', 'its a software farm\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `person_city`
--

CREATE TABLE `person_city` (
  `id` int(100) NOT NULL,
  `person_name` text COLLATE utf8_unicode_ci NOT NULL,
  `city_name` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `person_city`
--

INSERT INTO `person_city` (`id`, `person_name`, `city_name`) VALUES
(8, 'Ripon', 'chittagong'),
(9, 'Sourav', 'kumilla'),
(10, 'Rarim', 'dhaka');

-- --------------------------------------------------------

--
-- Table structure for table `person_gender`
--

CREATE TABLE `person_gender` (
  `id` int(100) NOT NULL,
  `person_name` text COLLATE utf8_unicode_ci NOT NULL,
  `person_gender` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `person_gender`
--

INSERT INTO `person_gender` (`id`, `person_name`, `person_gender`) VALUES
(8, 'Sourav Biswas', 'Male'),
(9, 'Dipannita', 'Female');

-- --------------------------------------------------------

--
-- Table structure for table `person_hobby`
--

CREATE TABLE `person_hobby` (
  `id` int(100) NOT NULL,
  `person_name` text COLLATE utf8_unicode_ci NOT NULL,
  `person_hobby` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `person_hobby`
--

INSERT INTO `person_hobby` (`id`, `person_name`, `person_hobby`) VALUES
(13, 'Mushfiq0', 'Dancing,Cricket,Writing'),
(14, 'Sourav Biswas', 'Programming,Singing,Cricket,Writing,Gardenning'),
(16, 'Sourav Biswas', 'Programming,Singing,Cricket'),
(17, 'Rahim', 'Cricket,Writing');

-- --------------------------------------------------------

--
-- Table structure for table `profile_picture`
--

CREATE TABLE `profile_picture` (
  `id` int(100) NOT NULL,
  `profile_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `profile_picture` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `profile_picture`
--

INSERT INTO `profile_picture` (`id`, `profile_name`, `profile_picture`) VALUES
(6, 'fghf', '1496051460wel1.JPG'),
(7, 'Sourav Biswas', '1496059019sourav.jpg'),
(13, 'Karim  mashfiq', ''),
(14, 'Karim', ''),
(15, 'dfdfd', '1496501617web-app.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `book_title`
--
ALTER TABLE `book_title`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `org_summary`
--
ALTER TABLE `org_summary`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `person_city`
--
ALTER TABLE `person_city`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `person_gender`
--
ALTER TABLE `person_gender`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `person_hobby`
--
ALTER TABLE `person_hobby`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profile_picture`
--
ALTER TABLE `profile_picture`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `book_title`
--
ALTER TABLE `book_title`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `org_summary`
--
ALTER TABLE `org_summary`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `person_city`
--
ALTER TABLE `person_city`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `person_gender`
--
ALTER TABLE `person_gender`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `person_hobby`
--
ALTER TABLE `person_hobby`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `profile_picture`
--
ALTER TABLE `profile_picture`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
